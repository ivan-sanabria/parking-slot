// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package server defines the api endpoints exposed to decentralize parking slot ticket generation, to fulfill
// requirements for different stakeholders of the parking-slot application.
package server

import (
	"bitbucket.org/ivan-sanabria/parking-slot/model"
	"bitbucket.org/ivan-sanabria/parking-slot/service"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
)

// Define constant for JSON content type as supported data-interchange format.
const jsonContentType = "application/json"
// Define constant for base search endpoint for cars
const carsSearchEndpoint = "/cars"

// Struct definition of car server used to inject car service for test driven development approach.
type CarServer struct {
	carService service.CarService
	http.Handler
}

// NewCarServer creates a new struct with api end points exposed to car clients.
func NewCarServer(carService service.CarService) *CarServer {

	carServer := new(CarServer)
	carServer.carService = carService

	router := mux.NewRouter().
		StrictSlash(true)

	sub := router.PathPrefix("/api/v1").
		Subrouter()

	sub.Methods(http.MethodPost).
		Path("/car").
		HandlerFunc(carServer.saveCar)

	sub.Methods(http.MethodPut).
		Path("/car/{uuid}").
		HandlerFunc(carServer.updateCar)

	sub.Methods(http.MethodGet).
		Path("/car/{uuid}").
		HandlerFunc(carServer.getCarByUuid)

	sub.Methods(http.MethodGet).
		Path(carsSearchEndpoint).
		Queries("registration", "{registration}").
		HandlerFunc(carServer.getCarsByRegistration)

	sub.Methods(http.MethodGet).
		Path(carsSearchEndpoint).
		Queries("color", "{color}").
		HandlerFunc(carServer.getCarsByColor)

	sub.Methods(http.MethodGet).
		Path(carsSearchEndpoint).
		Queries("model", "{model}").
		HandlerFunc(carServer.getCarsByModel)

	carServer.Handler = router

	return carServer
}

// saveCar function is responsible of extracting the car data from the http.request struct and passed to the car service
// for storing the data. After the service finishes, the function write header http.StatusCreated to the
// http.ResponseWriter interface, included on the http.response. In case, the extraction of car data from the
// http.request fails, the function sets http.error with http.StatusInternalServerError or http.StatusBadRequest
// codes and include the error on the response. This function is called when the http method the client uses is POST.
func (cs *CarServer) saveCar(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)

	if nil != err {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	car := new(model.Car)
	err = json.Unmarshal(body, car)

	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	if "" == car.Registration {
		http.Error(w, "Registration is missing on your request.", http.StatusBadRequest)
	}

	cs.carService.SaveCar(*car)
	w.Header().Set("Location",r.URL.Path + "/" + car.Uuid)
	w.WriteHeader(http.StatusCreated)
}

// updateCar function is responsible of extracting the car data from the http.request struct and passed to the car
// service for updating the record in datasource, when the given uuid on the endpoint exists. After the service finishes,
// the function write header http.StatusOK to the http.ResponseWriter interface, included on the http.response. In case,
// the given uuid does not exists the function return http.StatusNotFound to inform the client there is not hits.
// In case, the extraction of car data from the http.request fails, the function sets http.error with
// http.StatusInternalServerError or http.StatusBadRequest codes and include the error on the response. This function is
// called when the http method the client uses is PUT.
func (cs *CarServer) updateCar(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	oldCar, notFound := cs.carService.GetCarByUuid(uuid)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	body, err := ioutil.ReadAll(r.Body)

	if nil != err {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	newCar := new(model.Car)
	err = json.Unmarshal(body, newCar)

	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	cs.carService.UpdateCar(oldCar, *newCar)
	w.Header().Set("Location", r.URL.Path + "/" + oldCar.Uuid)
	w.WriteHeader(http.StatusOK)
}

// getCarByUuid function is responsible of extracting the car uuid from the http.request struct and passed to
// the car service for retrieving the car data. After the service finishes, the function write the given car data
// into response using the http.ResponseWriter interface. In case, there is not response from the car service,
// the function writes header http.StatusNotFound to inform the client there is not hits. This function is called when
// the http method the client uses is GET.
func (cs *CarServer) getCarByUuid(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	car, notFound := cs.carService.GetCarByUuid(uuid)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeCarJsonResponse(w, car)
}

// getCarByRegistration function is responsible of extracting the registration from the http.request struct and
// passed to the car service for retrieving the car data. After the service finishes, the function write the given car
// data into response using the http.ResponseWriter interface. In case, there is not response from the car service,
// the function writes header http.StatusNotFound to inform the client there is not hits. This function is called when
// the http method the client uses is GET.
func (cs *CarServer) getCarsByRegistration(w http.ResponseWriter, r *http.Request) {

	registration := r.FormValue("registration")

	if "" == registration {
		http.Error(w, "Empty car registration given on request.", http.StatusBadRequest)
	}

	cars, notFound := cs.carService.GetCarsByRegistration(registration)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeCarArrayJsonResponse(w, cars)
}

// getCarsByColor function is responsible of extracting the color from the http.request struct and passed to the car
// service for retrieving the car data. After the service finishes, the function write the given car data into response
// using the http.ResponseWriter interface. In case, there is not response from the car service, the function writes
// header http.StatusNotFound to inform the client there is not hits. This function is called when the http method the
// client uses is GET.
func (cs *CarServer) getCarsByColor(w http.ResponseWriter, r *http.Request) {

	color := r.FormValue("color")

	if "" == color {
		http.Error(w, "Empty car color given on request.", http.StatusBadRequest)
	}

	cars, notFound := cs.carService.GetCarsByColor(color)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeCarArrayJsonResponse(w, cars)
}

// getCarsByModel function is responsible of extracting the model from the http.request struct and passed to the car
// service for retrieving the car data. After the service finishes, the function write the given car data into response
// using the http.ResponseWriter interface. In case, there is not response from the car service, the function writes
// header http.StatusNotFound to inform the client there is not hits. This function is called when the http method the
// client uses is GET.
func (cs *CarServer) getCarsByModel(w http.ResponseWriter, r *http.Request) {

	carModel := r.FormValue("model")

	if "" == carModel {
		http.Error(w, "Empty car model given on request.", http.StatusBadRequest)
	}

	cars, notFound := cs.carService.GetCarsByModel(carModel)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeCarArrayJsonResponse(w, cars)
}

// writeCarJsonResponse function is responsible of parsing car data into JSON format and writing it on the
// given http.ResponseWriter interface.
func writeCarJsonResponse(w http.ResponseWriter, car model.Car) {

	bytes, _ := json.Marshal(car)

	w.Header().Set("Content-Type", jsonContentType)
	_, _ = w.Write(bytes)
}

// writeCarArrayJsonResponse function is responsible of parsing car array data into JSON format and writing it on the
// given http.ResponseWriter interface.
func writeCarArrayJsonResponse(w http.ResponseWriter, cars []model.Car) {

	bytes, _ := json.Marshal(cars)

	w.Header().Set("Content-Type", jsonContentType)
	_, _ = w.Write(bytes)
}