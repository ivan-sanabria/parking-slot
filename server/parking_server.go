// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"bitbucket.org/ivan-sanabria/parking-slot/model"
	"bitbucket.org/ivan-sanabria/parking-slot/service"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Struct definition of parking server used to inject parking service for test driven development approach.
type ParkingServer struct {
	parkingService service.ParkingService
	http.Handler
}

// NewParkingServer creates a new struct with api end points exposed to parking customers.
func NewParkingServer(parkingService service.ParkingService) *ParkingServer {

	parkingServer := new(ParkingServer)
	parkingServer.parkingService = parkingService

	router := mux.NewRouter().
		StrictSlash(true)

	sub := router.PathPrefix("/api/v1").
		Subrouter()

	sub.Methods(http.MethodPost).
		Path("/parking").
		HandlerFunc(parkingServer.saveParking)

	sub.Methods(http.MethodPut).
		Path("/parking/{uuid}").
		HandlerFunc(parkingServer.updateParking)

	sub.Methods(http.MethodGet).
		Path("/parking/{uuid}").
		HandlerFunc(parkingServer.getParkingByUuid)

	sub.Methods(http.MethodGet).
		Path("/parkings").
		Queries("latitude", "{latitude}", "longitude", "{longitude}").
		HandlerFunc(parkingServer.getParkingsByLocation)

	parkingServer.Handler = router

	return parkingServer
}

// saveParking function is responsible of extracting the parking data from the http.request struct and passed to
// the parking service for storing the data. After the service finishes, the function write header http.StatusCreated
// to the http.ResponseWriter interface, included on the http.response. In case, the extraction of parking data from
// the http.request fails, the function sets http.error with http.StatusInternalServerError or http.StatusBadRequest
// codes and include the error on the response. This function is called when the http method the client uses is POST.
func (p *ParkingServer) saveParking(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)

	if nil != err {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	parking := new(model.Parking)
	err = json.Unmarshal(body, parking)

	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	if 0 >= parking.Slots {
		http.Error(w, "Registration is missing on your request.", http.StatusBadRequest)
	}

	p.parkingService.SaveParking(*parking)
	w.Header().Set("Location",r.URL.Path + "/" + parking.Uuid)
	w.WriteHeader(http.StatusCreated)
}

// updateParking function is responsible of extracting the parking data from the http.request struct and passed to
// the parking service for updating the record in datasource, when the given uuid on the endpoint exists.
// After the service finishes, the function write header StatusOK to the http.ResponseWriter interface, included on
// the http.response. In case, the extraction of parking data from the http.request fails, the function sets http.error
// with http.StatusInternalServerError or http.StatusBadRequest codes and include the error on the response.
// This function is called when the http method the client uses is PUT.
func (p *ParkingServer) updateParking(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	oldParking, notFound := p.parkingService.GetParkingByUuid(uuid)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	body, err := ioutil.ReadAll(r.Body)

	if nil != err {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	newParking := new(model.Parking)
	err = json.Unmarshal(body, newParking)

	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	p.parkingService.UpdateParking(oldParking, *newParking)
	w.Header().Set("Location", r.URL.Path + "/" + oldParking.Uuid)
	w.WriteHeader(http.StatusOK)
}

// getParkingByUuid function is responsible of extracting the parking uuid from the http.request struct and passed to
// the parking service for retrieving the parking data. After the service finishes, the function write the given parking
// data into response using the http.ResponseWriter interface. In case, there is not response from the parking service,
// the function writes header http.StatusNotFound to inform the client there is not hits. This function is called when
// the http method the client uses is GET.
func (p *ParkingServer) getParkingByUuid(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	parking, notFound := p.parkingService.GetParkingByUuid(uuid)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeParkingJsonResponse(w, parking)
}

// getParkingsByLocation function is responsible of extracting the latitude and longitude from the http.request struct
// and passed to the parking service for retrieving the parking data. After the service finishes, the function write
// the given parking data into response using the http.ResponseWriter interface. In case, there is not response from
// the parking service, the function writes header http.StatusNotFound to inform the client there is not hits.
// In case, the extraction of latitude or longitude from the http.request fails, the function sets http.error with
// http.StatusInternalServerError or http.StatusBadRequest codes and include the error on the response. This function
// is called when the http method the client uses is GET.
func (p *ParkingServer) getParkingsByLocation(w http.ResponseWriter, r *http.Request) {

	latitude, err := strconv.ParseFloat(r.FormValue("latitude"), 64)

	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	longitude, err := strconv.ParseFloat(r.FormValue("longitude"), 64)

	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	parking, notFound := p.parkingService.GetParkingByLocation(latitude, longitude)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeParkingArrayJsonResponse(w, parking)
}

// writeParkingJsonResponse function is responsible of parsing parking data into JSON format and writing it on the
// given http.ResponseWriter interface.
func writeParkingJsonResponse(w http.ResponseWriter, parking model.Parking) {

	bytes, _ := json.Marshal(parking)

	w.Header().Set("Content-Type", jsonContentType)
	_, _ = w.Write(bytes)
}

// writeParkingArrayJsonResponse function is responsible of parsing parking array data into JSON format and writing it
// on the given http.ResponseWriter interface.
func writeParkingArrayJsonResponse(w http.ResponseWriter, parkings []model.Parking) {

	bytes, _ := json.Marshal(parkings)

	w.Header().Set("Content-Type", jsonContentType)
	_, _ = w.Write(bytes)
}