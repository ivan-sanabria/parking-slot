// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"bitbucket.org/ivan-sanabria/parking-slot/model"
	"fmt"
	"math"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// Define constant for parking api base endpoint to cover GET and PUT operations
const parkingApiGetPut = "/api/v1/parking/%s"
// Define constant for parking api base endpoint to cover POST operation
const parkingApiPost = "/api/v1/parking"
// Define constant for parking search endpoint
const parkingsApiGet = "/api/v1/parkings"

// Define EPSILON for float comparison
const EPSILON float64 = 0.00000001

// Struct definition to mock the parking service
type MockParkingService struct {
	parkingDataset map[string]model.Parking
}

// Mock functionality of SaveParking function.
func (mps *MockParkingService) SaveParking(parking model.Parking) {
	mps.parkingDataset[parking.Uuid] = parking
}

// Mock functionality of UpdateParking function.
func (mps *MockParkingService) UpdateParking(oldParking model.Parking, newParking model.Parking) {
	mps.parkingDataset[oldParking.Uuid] = newParking
}

// Mock functionality of GetParkingByUuid function.
func (mps *MockParkingService) GetParkingByUuid(uuid string) (model.Parking, bool) {

	parking, found := mps.parkingDataset[uuid]

	return parking, !found
}

// Mock functionality of GetParkingByLocation function.
func (mps *MockParkingService) GetParkingByLocation(latitude float64, longitude float64) ([] model.Parking, bool) {

	var results [] model.Parking

	for _, value := range mps.parkingDataset {

		if (EPSILON >= math.Abs(latitude - value.Latitude)) && (EPSILON >= math.Abs(longitude - value.Longitude)) {

			results = append(results, value)
		}
	}

	return results, len(results) == 0
}

// Define the suite is going to be cover for SaveParking function of parking_server.
func TestSaveParking(t *testing.T) {

	parkingService := MockParkingService {
		map[string] model.Parking{},
	}

	server := NewParkingServer(&parkingService)

	t.Run("save parking with uuid-1", func(t *testing.T) {

		body := strings.NewReader(
			"{\"uuid\": \"uuid-1\", \"slots\": 10, \"latitude\": 52.5201, \"longitude\": 13.4050}")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusCreated != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusCreated)
		}
	})

	t.Run("save parking with missing number of slots", func(t *testing.T) {

		body := strings.NewReader(
			"{\"uuid\": \"uuid-1\", \"latitude\": 52.5201, \"longitude\": 13.4050}")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("save parking with invalid json input", func(t *testing.T) {

		body := strings.NewReader(
			"Hello Ladies and Gentleman this is not going to work!!!!")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("save parking with invalid body request", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingApiPost), errReader(0))
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusInternalServerError != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusInternalServerError)
		}
	})
}

// Define the suite is going to be cover for UpdateParking function of parking_server.
func TestUpdateParking(t *testing.T) {

	existingKey := "uuid-2"
	expectedLatitude := 48.8566
	expectedLongitude := 2.3522

	parkingService := MockParkingService {
		map[string] model.Parking {
			existingKey: {existingKey, 10, 52.5201, 13.4050},
		},
	}

	server := NewParkingServer(&parkingService)

	body := strings.NewReader("{\"slots\": 12, \"latitude\": 48.8566, \"longitude\": 2.3522}")

	t.Run("update parking with " + existingKey, func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(parkingApiGetPut, existingKey), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		slots := parkingService.parkingDataset[existingKey].Slots

		if 12 != slots {
			t.Errorf("Slots number is %d different than expected on service dataset.", slots)
		}

		latitude := parkingService.parkingDataset[existingKey].Latitude

		if EPSILON < math.Abs(expectedLatitude - latitude) {
			t.Errorf("Latitude is %f different than expected on service dataset.", latitude)
		}

		longitude := parkingService.parkingDataset[existingKey].Longitude

		if EPSILON < math.Abs(expectedLongitude - longitude) {
			t.Errorf("Longitude is %f different than expected on service dataset.", longitude)
		}
	})

	t.Run("update parking does not exists on dataset", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(parkingApiGetPut, "uuid-3"), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("update parking with invalid input json", func(t *testing.T) {

		body := strings.NewReader("Au revoir")

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(parkingApiGetPut, existingKey), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("update parking with invalid body request", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(parkingApiGetPut, existingKey), errReader(0))
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusInternalServerError != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusInternalServerError)
		}
	})
}

// Define the suite is going to be cover for GetParkingByUuid function of parking_server.
func TestGetParkingByUuid(t *testing.T) {

	existingKey := "uuid-5"

	parkingService := MockParkingService {
		map[string] model.Parking {
			existingKey: {existingKey, 10, float64(1.0), float64(1.0)},
		},
	}

	server := NewParkingServer(&parkingService)

	t.Run("get parking by uuid and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(parkingApiGetPut, existingKey), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "{\"uuid\":\"uuid-5\",\"slots\":10,\"latitude\":1,\"longitude\":1}"
		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get parking by uuid and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(parkingApiGetPut, "uuid-6"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})
}

// Define the suite is going to be cover for GetParkingBy function of parking_server.
func TestGetParkingByLocation(t *testing.T) {

	existingLatitude := 52.5201
	existingLongitude := 13.4050

	parkingService := MockParkingService {
		map[string] model.Parking {
			"uuid-10": {"uuid-10", 15, existingLatitude, existingLongitude},
			"uuid-11": {"uuid-11", 20, existingLatitude, existingLongitude},
			"uuid-12": {"uuid-12", 5, 48.8566, 2.3522},
		},
	}

	server := NewParkingServer(&parkingService)
	locationEndpoint := parkingsApiGet + "?latitude=%f&longitude=%f"

	t.Run("get parking by location and retrieves multiple records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(locationEndpoint, existingLatitude, existingLongitude), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody1 := "[{\"uuid\":\"uuid-10\",\"slots\":15,\"latitude\":52.5201,\"longitude\":13.405}," +
			"{\"uuid\":\"uuid-11\",\"slots\":20,\"latitude\":52.5201,\"longitude\":13.405}]"

		expectedBody2 := "[{\"uuid\":\"uuid-11\",\"slots\":20,\"latitude\":52.5201,\"longitude\":13.405}," +
			"{\"uuid\":\"uuid-10\",\"slots\":15,\"latitude\":52.5201,\"longitude\":13.405}]"

		body := response.Body.String()

		if (expectedBody1 != body) && (expectedBody2 != body) {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get parking by location and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(locationEndpoint, 48.8566, 2.3522), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "[{\"uuid\":\"uuid-12\",\"slots\":5,\"latitude\":48.8566,\"longitude\":2.3522}]"

		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get parking by location and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(locationEndpoint, 1.0, 1.0), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("get parking by location and response is bad request because latitude is invalid", func(t *testing.T) {

		locationEndpoint := parkingsApiGet + "?latitude=%s&longitude=%f"

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(locationEndpoint, "aaaa", 1.0), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("get parking by location and response is bad request because longitude is invalid", func(t *testing.T) {

		locationEndpoint := parkingsApiGet + "?latitude=%f&longitude=%s"

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(locationEndpoint, 1.0, "bbbb"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})
}