// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package server defines the api endpoints exposed to decentralize parking slot ticket generation, to fulfill
// requirements for different stakeholders of the parking-slot application.
package server

import (
	"bitbucket.org/ivan-sanabria/parking-slot/model"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// Define constant for car api base endpoint to cover GET and PUT operations
const carApiGetPut = "/api/v1/car/%s"
// Define constant for car api base endpoint to cover POST operation
const carApiPost = "/api/v1/car"
// Define constant for car search endpoint
const carsApiGet = "/api/v1/cars"

// Define constant for error messages for HTTP codes
const errorHttpCodes = "Response code is %d different than expected %d."
// Define constant for error messages for body differences
const errorBodyContent = "Response body is %s different than expected."

// Struct definition to mock the car service
type MockCarService struct {
	carDataset map[string]model.Car
}

// Mock functionality of SaveCar function.
func (mcs *MockCarService) SaveCar(car model.Car) {
	mcs.carDataset[car.Uuid] = car
}

// Mock functionality of UpdateCar function.
func (mcs *MockCarService) UpdateCar(oldCar model.Car, newCar model.Car) {
	mcs.carDataset[oldCar.Uuid] = newCar
}

// Mock functionality of GetCarByUuid function.
func (mcs *MockCarService) GetCarByUuid(uuid string) (model.Car, bool) {

	car, found := mcs.carDataset[uuid]

	return car, !found
}

// Mock functionality of GetCarsByRegistration function.
func (mcs *MockCarService) GetCarsByRegistration(registration string) ([] model.Car, bool) {

	var results [] model.Car

	for _, value := range mcs.carDataset {

		if registration == value.Registration {
			results = append(results, value)
		}
	}

	return results, len(results) == 0
}

// Mock functionality of GetCarsByColor function.
func (mcs *MockCarService) GetCarsByColor(color string) ([] model.Car, bool) {

	var results [] model.Car

	for _, value := range mcs.carDataset {

		if color == value.Color {
			results = append(results, value)
		}
	}

	return results, len(results) == 0
}

// Mock functionality of GetCarsByModel function.
func (mcs *MockCarService) GetCarsByModel(modelCar string) ([] model.Car, bool) {

	var results [] model.Car

	for _, value := range mcs.carDataset {

		if modelCar == value.Model {
			results = append(results, value)
		}
	}

	return results, len(results) == 0
}

// Definition of the errReader mock for simulating ioutil errors.
type errReader int

// Define functions that simulate a corrupted body that is going to travel on http request.
func (errReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("test error")
}

// Define the suite is going to be cover for SaveCar function of car_server.
func TestSaveCar(t *testing.T) {

	carService := MockCarService {
		map[string] model.Car {},
	}

	server := NewCarServer(&carService)

	t.Run("save car with uuid-3", func(t *testing.T) {

		body := strings.NewReader(
			"{\"uuid\": \"uuid-3\", \"registration\": \"CCCC\", \"color\": \"Blue\", \"model\": \"Jetta\"}")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(carApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusCreated != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusCreated)
		}
	})

	t.Run("save car with invalid input missing registration", func(t *testing.T) {

		body := strings.NewReader("{\"uuid\": \"uuid-4\", \"color\": \"Blue\", \"model\": \"Jetta\"}")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(carApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("save car with invalid input json", func(t *testing.T) {

		body := strings.NewReader("Good morning everyone this is not working...")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(carApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("save car with invalid body request", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(carApiPost), errReader(0))
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusInternalServerError != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusInternalServerError)
		}
	})
}

// Define the suite is going to be cover for UpdateCar function of car_server.
func TestUpdateCar(t *testing.T) {

	existingKey := "uuid-2"

	carService := MockCarService {
		map[string] model.Car {
			existingKey: {existingKey, "BBBB", "Red", "Passat"},
			"uuid-4": {"uuid-4", "DDDD", "Purple", "Wagon"},
		},
	}

	server := NewCarServer(&carService)

	body := strings.NewReader("{\"registration\": \"CCCC\", \"color\": \"Blue\", \"model\": \"Jetta\"}")

	t.Run("update car with " + existingKey, func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(carApiGetPut, existingKey), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		registration := carService.carDataset[existingKey].Registration

		if "CCCC" !=  registration {
			t.Errorf("Registration is %s different than expected on service dataset.", registration)
		}

		color := carService.carDataset[existingKey].Color

		if "Blue" !=  color {
			t.Errorf("Color is %s different than expected on service dataset.", color)
		}

		modelCar := carService.carDataset[existingKey].Model

		if "Jetta" !=  modelCar {
			t.Errorf("Model is %s different than expected on service dataset.", modelCar)
		}
	})

	t.Run("update car does not exists on dataset", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(carApiGetPut, "uuid-3"), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("update car with invalid input json", func(t *testing.T) {

		body := strings.NewReader("Good morning everyone this is not working...")

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(carApiGetPut, existingKey), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("update car with invalid body request", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(carApiGetPut, existingKey), errReader(0))
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusInternalServerError != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusInternalServerError)
		}
	})
}

// Define the suite is going to be cover for GetCarByUuid function of car_server.
func TestGetCarByUuid(t *testing.T) {

	existingKey := "uuid-5"

	carService := MockCarService {
		map[string] model.Car {
			existingKey : {existingKey, "BBBB", "Brown", "Polo"},
		},
	}

	server := NewCarServer(&carService)

	t.Run("get car by uuid and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(carApiGetPut, existingKey), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "{\"uuid\":\"uuid-5\",\"registration\":\"BBBB\",\"color\":\"Brown\",\"model\":\"Polo\"}"
		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get car by uuid and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(carApiGetPut, "uuid-6"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})
}

// Define the suite is going to be cover for GetCarsByRegistration function of car_server.
func TestGetCarsByRegistration(t *testing.T) {

	existingRegistration := "HHHH"

	carService := MockCarService {
		map[string] model.Car {
			"uuid-10": {"uuid-10", existingRegistration, "Yellow", "Touran"},
			"uuid-11": {"uuid-11", "LLLL", "Blue", "Touareg"},
			"uuid-12": {"uuid-12", existingRegistration, "Red", "Sharan"},
		},
	}

	server := NewCarServer(&carService)
	registrationEndpoint := carsApiGet + "?registration=%s"

	t.Run("get cars by registration and retrieves multiple records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(registrationEndpoint, existingRegistration), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody1 := "[{\"uuid\":\"uuid-10\",\"registration\":\"HHHH\",\"color\":\"Yellow\",\"model\":\"Touran\"}," +
			"{\"uuid\":\"uuid-12\",\"registration\":\"HHHH\",\"color\":\"Red\",\"model\":\"Sharan\"}]"

		expectedBody2 := "[{\"uuid\":\"uuid-12\",\"registration\":\"HHHH\",\"color\":\"Red\",\"model\":\"Sharan\"}," +
			"{\"uuid\":\"uuid-10\",\"registration\":\"HHHH\",\"color\":\"Yellow\",\"model\":\"Touran\"}]"

		body := response.Body.String()

		if (expectedBody1 != body) && (expectedBody2 != body) {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get cars by registration and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(registrationEndpoint, "LLLL"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "[{\"uuid\":\"uuid-11\",\"registration\":\"LLLL\",\"color\":\"Blue\",\"model\":\"Touareg\"}]"
		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get car by registration and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(registrationEndpoint, "WTF"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("get car by empty registration", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(registrationEndpoint, ""), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})
}

// Define the suite is going to be cover for GetCarsByColor function of car_server.
func TestGetCarsByColor(t *testing.T) {

	existingColor := "Silver"

	carService := MockCarService {
		map[string] model.Car {
			"uuid-20": {"uuid-20", "XXXX", "Green", "Tiguan"},
			"uuid-21": {"uuid-21", "YYYY", existingColor, "Caddy"},
			"uuid-22": {"uuid-22", "ZZZZ", existingColor, "T-Roc"},
		},
	}

	server := NewCarServer(&carService)
	colorEndpoint := carsApiGet + "?color=%s"

	t.Run("get cars by color and retrieves multiple records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(colorEndpoint, existingColor), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody1 := "[{\"uuid\":\"uuid-21\",\"registration\":\"YYYY\",\"color\":\"Silver\",\"model\":\"Caddy\"}," +
			"{\"uuid\":\"uuid-22\",\"registration\":\"ZZZZ\",\"color\":\"Silver\",\"model\":\"T-Roc\"}]"

		expectedBody2 := "[{\"uuid\":\"uuid-22\",\"registration\":\"ZZZZ\",\"color\":\"Silver\",\"model\":\"T-Roc\"}," +
			"{\"uuid\":\"uuid-21\",\"registration\":\"YYYY\",\"color\":\"Silver\",\"model\":\"Caddy\"}]"

		body := response.Body.String()

		if (expectedBody1 != body) && (expectedBody2 != body) {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get cars by color and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(colorEndpoint, "Green"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "[{\"uuid\":\"uuid-20\",\"registration\":\"XXXX\",\"color\":\"Green\",\"model\":\"Tiguan\"}]"
		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get car by color and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(colorEndpoint, "WTF"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("get car by empty color", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(colorEndpoint, ""), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})
}

// Define the suite is going to be cover for GetCarsByModel function of car_server.
func TestGetCarsByModel(t *testing.T) {

	existingModel := "T-Cross"

	carService := MockCarService {
		map[string] model.Car {
			"uuid-30": {"uuid-30", "RRRR", "Gold", existingModel},
			"uuid-31": {"uuid-31", "SSSS", "Cyan", "Golf-1"},
			"uuid-32": {"uuid-32", "TTTT", "Orange", existingModel},
		},
	}

	server := NewCarServer(&carService)
	modelEndpoint := carsApiGet + "?model=%s"

	t.Run("get cars by model and retrieves multiple records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(modelEndpoint, existingModel), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody1 := "[{\"uuid\":\"uuid-30\",\"registration\":\"RRRR\",\"color\":\"Gold\",\"model\":\"T-Cross\"}," +
			"{\"uuid\":\"uuid-32\",\"registration\":\"TTTT\",\"color\":\"Orange\",\"model\":\"T-Cross\"}]"

		expectedBody2 := "[{\"uuid\":\"uuid-32\",\"registration\":\"TTTT\",\"color\":\"Orange\",\"model\":\"T-Cross\"}," +
			"{\"uuid\":\"uuid-30\",\"registration\":\"RRRR\",\"color\":\"Gold\",\"model\":\"T-Cross\"}]"

		body := response.Body.String()

		if (expectedBody1 != body) && (expectedBody2 != body) {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get cars by model and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(modelEndpoint, "Golf-1"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "[{\"uuid\":\"uuid-31\",\"registration\":\"SSSS\",\"color\":\"Cyan\",\"model\":\"Golf-1\"}]"
		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get car by model and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(modelEndpoint, "WTF"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("get car by empty model", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(modelEndpoint, ""), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})
}