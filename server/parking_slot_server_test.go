// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"bitbucket.org/ivan-sanabria/parking-slot/model"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// Define constant for parking slot api base endpoint to cover GET and PUT operations
const parkingSlotApiGetPut = "/api/v1/parking-slot/%s"
// Define constant for parking slot api base endpoint to cover POST operation
const parkingSlotApiPost = "/api/v1/parking-slot"
// Define constant for parking slot search endpoint
const parkingSlotsApiSearch = "/api/v1/parking-slot?"

// Struct definition to mock the parking slot service
type MockParkingSlotService struct {
	parkingSlotDataset map[string]model.ParkingSlot
}

// Mock functionality of SaveParkingSlot function.
func (mps *MockParkingSlotService) SaveParkingSlot(parkingSlot model.ParkingSlot) {
	mps.parkingSlotDataset[parkingSlot.Uuid] = parkingSlot
}

// Mock functionality of UpdateParkingSlot function.
func (mps *MockParkingSlotService) UpdateParkingSlot(oldParkingSlot model.ParkingSlot, newParkingSlot model.ParkingSlot) {
	mps.parkingSlotDataset[oldParkingSlot.Uuid] = newParkingSlot
}

// Mock functionality of GetParkingSlotByUuid function.
func (mps *MockParkingSlotService) GetParkingSlotByUuid(uuid string) (model.ParkingSlot, bool) {

	parkingSlot, found := mps.parkingSlotDataset[uuid]

	return parkingSlot, !found
}

// Mock functionality of GetParkingSlotByCarUuid function.
func (mps *MockParkingSlotService) GetParkingSlotByCarUuid(carUuid string) (model.ParkingSlot, bool) {

	for _, value := range mps.parkingSlotDataset {

		if carUuid == value.CarUuid {
			return value, false
		}
	}

	return model.ParkingSlot{}, true
}

// Mock functionality of GetParkingSlotsByParkingUuid function.
func (mps *MockParkingSlotService) GetParkingSlotsByParkingUuid(parkingUuid string) ([] model.ParkingSlot, bool) {

	var results [] model.ParkingSlot

	for _, value := range mps.parkingSlotDataset {

		if parkingUuid == value.ParkingUuid {
			results = append(results, value)
		}
	}

	return results, len(results) == 0
}

// Define the suite is going to be cover for SaveParkingSlot function of parking_slot_server.
func TestSaveParkingSlot(t *testing.T) {

	parkingSlotService := MockParkingSlotService {
		map[string] model.ParkingSlot{},
	}

	server := NewParkingSlotServer(&parkingSlotService)

	t.Run("save parking slot with uuid-1", func(t *testing.T) {

		body := strings.NewReader(
			"{\"uuid\": \"uuid-1\", \"parkingUuid\": \"park-uuid-1\", \"slotId\": 1}")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingSlotApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusCreated != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusCreated)
		}
	})

	t.Run("save parking slot with missing parking unique identifier", func(t *testing.T) {

		body := strings.NewReader(
			"{\"uuid\": \"uuid-1\", \"slotId\": 1}")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingSlotApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("save parking slot with missing slot identifier", func(t *testing.T) {

		body := strings.NewReader(
			"{\"uuid\": \"uuid-1\", \"parkingUuid\": \"park-uuid-1\"}")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingSlotApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("save parking slot with invalid json input", func(t *testing.T) {

		body := strings.NewReader(
			"Let's go to rock and roll //")

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingSlotApiPost), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("save parking slot with invalid body request", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf(parkingSlotApiPost), errReader(0))
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusInternalServerError != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusInternalServerError)
		}
	})
}

// Define the suite is going to be cover for UpdateParkingSlot function of parking_slot_server.
func TestUpdateParkingSlot(t *testing.T) {

	existingKey := "uuid-2"

	parkingSlotService := MockParkingSlotService {
		map[string] model.ParkingSlot {
			existingKey: {existingKey, "parking-uuid-1", 1, "", 0, 0},
		},
	}

	server := NewParkingSlotServer(&parkingSlotService)

	body := strings.NewReader("{\"slotId\": 2}")

	t.Run("update parking slot with " + existingKey, func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(parkingSlotApiGetPut, existingKey), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		slotId := parkingSlotService.parkingSlotDataset[existingKey].SlotId

		if 2 != slotId {
			t.Errorf("Slot Identifier is %d different than expected on service dataset.", slotId)
		}
	})

	t.Run("update parking slot does not exists on dataset", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(parkingSlotApiGetPut, "uuid-3"), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("update parking slot with invalid input json", func(t *testing.T) {

		body := strings.NewReader("auf wiedersehen")

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(parkingSlotApiGetPut, existingKey), body)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})

	t.Run("update parking slot with invalid body request", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf(parkingSlotApiGetPut, existingKey), errReader(0))
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusInternalServerError != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusInternalServerError)
		}
	})
}

// Define the suite is going to be cover for GetParkingSlotByUuid function of parking_slot_server.
func TestGetParkingSlotByUuid(t *testing.T) {

	existingKey := "uuid-5"

	parkingSlotService := MockParkingSlotService {
		map[string] model.ParkingSlot {
			existingKey: {existingKey, "parking-uuid-1", 2, "", 0, 0},
		},
	}

	server := NewParkingSlotServer(&parkingSlotService)

	t.Run("get parking slot by uuid and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(parkingSlotApiGetPut, existingKey), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "{\"uuid\":\"uuid-5\",\"parkingUuid\":\"parking-uuid-1\",\"slotId\":2,\"carUuid\":\"\",\"checkIn\":0,\"checkOut\":0}"
		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get parking slot by uuid and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(parkingSlotApiGetPut, "uuid-6"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})
}

// Define the suite is going to be cover for GetParkingSlotByCarUuid function of parking_slot_server.
func TestGetParkingSlotByCarUuid(t *testing.T) {

	existingCarUuid := "car-uuid-1"

	parkingSlotService := MockParkingSlotService {
		map[string] model.ParkingSlot {
			"uuid-10": {"uuid-10", "parking-uuid-1", 1, existingCarUuid, 1, 0},
			"uuid-11": {"uuid-11", "parking-uuid-1", 2, "", 0, 0},
			"uuid-12": {"uuid-12", "parking-uuid-2", 1, "car-uuid-2", 0, 0},
		},
	}

	server := NewParkingSlotServer(&parkingSlotService)
	searchByCarEndpoint := parkingSlotsApiSearch + "car=%s"

	t.Run("get parking slot by car uuid and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(searchByCarEndpoint, existingCarUuid), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "{\"uuid\":\"uuid-10\",\"parkingUuid\":\"parking-uuid-1\",\"slotId\":1,\"carUuid\":\"car-uuid-1\",\"checkIn\":1,\"checkOut\":0}"
		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get parking slot by car uuid and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(searchByCarEndpoint, "Iron-Man"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("get parking slot by empty car uuid", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(searchByCarEndpoint, ""), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})
}

// Define the suite is going to be cover for GetParkingSlotByParkingUuid function of parking_slot_server.
func TestGetParkingSlotByParkingUuid(t *testing.T) {

	existingParkingUuid := "parking-uuid-1"

	parkingSlotService := MockParkingSlotService {
		map[string] model.ParkingSlot {
			"uuid-20": {"uuid-20", existingParkingUuid, 1, "car-uuid-10", 1, 0},
			"uuid-21": {"uuid-21", "parking-uuid-2", 1, "car-uuid-30", 1, 0},
			"uuid-22": {"uuid-22", existingParkingUuid, 2, "car-uuid-20", 2, 0},
		},
	}

	server := NewParkingSlotServer(&parkingSlotService)
	searchByParkingEndpoint := parkingSlotsApiSearch + "parking=%s"

	t.Run("get parking slot by parking uuid and retrieves multiple records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(searchByParkingEndpoint, existingParkingUuid), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody1 := "[{\"uuid\":\"uuid-20\",\"parkingUuid\":\"parking-uuid-1\",\"slotId\":1,\"carUuid\":\"car-uuid-10\",\"checkIn\":1,\"checkOut\":0}," +
			"{\"uuid\":\"uuid-22\",\"parkingUuid\":\"parking-uuid-1\",\"slotId\":2,\"carUuid\":\"car-uuid-20\",\"checkIn\":2,\"checkOut\":0}]"

		expectedBody2 := "[{\"uuid\":\"uuid-22\",\"parkingUuid\":\"parking-uuid-1\",\"slotId\":2,\"carUuid\":\"car-uuid-20\",\"parkIn\":2,\"parkOut\":0}," +
			"{\"uuid\":\"uuid-20\",\"parkingUuid\":\"parking-uuid-1\",\"slotId\":1,\"carUuid\":\"car-uuid-10\",\"checkIn\":1,\"checkOut\":0}]"

		body := response.Body.String()

		if (expectedBody1 != body) && (expectedBody2 != body) {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get parking slot by parking uuid and retrieves one record", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(searchByParkingEndpoint, "parking-uuid-2"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusOK != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusOK)
		}

		expectedBody := "[{\"uuid\":\"uuid-21\",\"parkingUuid\":\"parking-uuid-2\",\"slotId\":1,\"carUuid\":\"car-uuid-30\",\"checkIn\":1,\"checkOut\":0}]"
		body := response.Body.String()

		if expectedBody != body {
			t.Errorf(errorBodyContent, body)
		}
	})

	t.Run("get parking slot by parking uuid and retrieves zero records", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(searchByParkingEndpoint, "Bond-007"), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusNotFound != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusNotFound)
		}
	})

	t.Run("get parking slot by empty parking uuid", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf(searchByParkingEndpoint, ""), nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		if http.StatusBadRequest != response.Code {
			t.Errorf(errorHttpCodes, response.Code, http.StatusBadRequest)
		}
	})
}