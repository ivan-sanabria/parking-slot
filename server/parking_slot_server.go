// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"bitbucket.org/ivan-sanabria/parking-slot/model"
	"bitbucket.org/ivan-sanabria/parking-slot/service"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
)

// Define constant for parking slot api base endpoint
const parkingSlotApi = "/parking-slot"

// Struct definition of parking slot server used to inject parking slot service for test driven development approach.
type ParkingSlotServer struct {
	parkingSlotService service.ParkingSlotService
	http.Handler
}

// NewParkingSlotServer creates a new struct with api end points exposed to parking slot clients.
func NewParkingSlotServer(parkingSlotService service.ParkingSlotService) *ParkingSlotServer {

	parkingSlotServer := new(ParkingSlotServer)
	parkingSlotServer.parkingSlotService = parkingSlotService

	router := mux.NewRouter().
		StrictSlash(true)

	sub := router.PathPrefix("/api/v1").
		Subrouter()

	sub.Methods(http.MethodPost).
		Path(parkingSlotApi).
		HandlerFunc(parkingSlotServer.saveParkingSlot)

	sub.Methods(http.MethodPut).
		Path("/parking-slot/{uuid}").
		HandlerFunc(parkingSlotServer.updateParkingSlot)

	sub.Methods(http.MethodGet).
		Path("/parking-slot/{uuid}").
		HandlerFunc(parkingSlotServer.getParkingSlotByUuid)

	sub.Methods(http.MethodGet).
		Path(parkingSlotApi).
		Queries("car", "{car}").
		HandlerFunc(parkingSlotServer.getParkingSlotByCarUuid)

	sub.Methods(http.MethodGet).
		Path(parkingSlotApi).
		Queries("parking", "{parking}").
		HandlerFunc(parkingSlotServer.getParkingSlotsByParkingUuid)

	parkingSlotServer.Handler = router

	return parkingSlotServer
}

// saveParkingSlot function is responsible of extracting the parking slot data from the http.request struct and passed
// to the parking slot service for storing the data. After the service finishes, the function write header
// http.StatusCreated to the http.ResponseWriter interface, included on the http.response. In case, the extraction of
// parking slot data from the http.request fails, the function sets http.error with http.StatusInternalServerError or
// http.StatusBadRequest codes and include the error on the response. This function is called when the http method
// the client uses is POST.
func (ps *ParkingSlotServer) saveParkingSlot(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)

	if nil != err {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	parkingSlot := new(model.ParkingSlot)
	err = json.Unmarshal(body, parkingSlot)

	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	if "" == parkingSlot.ParkingUuid {
		http.Error(w, "Parking unique identifier is missing on your request.", http.StatusBadRequest)
	}

	if 0 >= parkingSlot.SlotId {
		http.Error(w, "Slot identifier is missing on your request.", http.StatusBadRequest)
	}

	ps.parkingSlotService.SaveParkingSlot(*parkingSlot)
	w.Header().Set("Location",r.URL.Path + "/" + parkingSlot.Uuid)
	w.WriteHeader(http.StatusCreated)
}

// updateParkingSlot function is responsible of extracting the parking slot data from the http.request struct and passed
// to the parking slot service for updating the record in datasource, when the given uuid on the endpoint exists.
// After the service finishes, the function write header StatusOK to the http.ResponseWriter interface, included on
// the http.response. In case, the extraction of parking slot data from the http.request fails, the function sets
// http.error with http.StatusInternalServerError or http.StatusBadRequest codes and include the error on the response.
// This function is called when the http method the client uses is PUT.
func (ps *ParkingSlotServer) updateParkingSlot(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	oldParkingSlot, notFound := ps.parkingSlotService.GetParkingSlotByUuid(uuid)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	body, err := ioutil.ReadAll(r.Body)

	if nil != err {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	newParkingSlot := new(model.ParkingSlot)
	err = json.Unmarshal(body, newParkingSlot)

	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	ps.parkingSlotService.UpdateParkingSlot(oldParkingSlot, *newParkingSlot)
	w.Header().Set("Location", r.URL.Path + "/" + oldParkingSlot.Uuid)
	w.WriteHeader(http.StatusOK)
}

// getParkingSlotByUuid function is responsible of extracting the parking slot uuid from the http.request struct and
// passed to the parking slot service for retrieving the parking slot data. After the service finishes, the function
// write the given parking slot data into response using the http.ResponseWriter interface. In case, there is not
// response from the parking slot service, the function writes header http.StatusNotFound to inform the client there is
// not hits. This function is called when the http method the client uses is GET.
func (ps *ParkingSlotServer) getParkingSlotByUuid(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	parkingSlot, notFound := ps.parkingSlotService.GetParkingSlotByUuid(uuid)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeParkingSlotJsonResponse(w, parkingSlot)
}

// getParkingSlotByCarUuid function is responsible of extracting the car uuid from the http.request struct and passed to
// the parking slot service for retrieving the parking slot data. After the service finishes, the function write the
// given parking slot data into response using the http.ResponseWriter interface. In case, there is not response from
// the parking slot service, the function writes header http.StatusNotFound to inform the client there is not hits.
// This function is called when the http method the client uses is GET.
func (ps *ParkingSlotServer) getParkingSlotByCarUuid(w http.ResponseWriter, r *http.Request) {

	carUuid := r.FormValue("car")

	if "" == carUuid {
		http.Error(w, "Empty car uuid given on request.", http.StatusBadRequest)
	}

	parkingSlot, notFound := ps.parkingSlotService.GetParkingSlotByCarUuid(carUuid)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeParkingSlotJsonResponse(w, parkingSlot)
}

// getParkingSlotsByParkingUuid function is responsible of extracting the parking uuid from the http.request struct and
// passed to the parking slot service for retrieving the parking slot data. After the service finishes, the function
// write the given parking slot data into response using the http.ResponseWriter interface. In case, there is not
// response from the parking slot service, the function writes header http.StatusNotFound to inform the client there is
// not hits. This function is called when the http method the client uses is GET.
func (ps *ParkingSlotServer) getParkingSlotsByParkingUuid(w http.ResponseWriter, r *http.Request) {

	parkingUuid := r.FormValue("parking")

	if "" == parkingUuid {
		http.Error(w, "Empty parking uuid given on request.", http.StatusBadRequest)
	}

	parkingSlots, notFound := ps.parkingSlotService.GetParkingSlotsByParkingUuid(parkingUuid)

	if notFound {
		w.WriteHeader(http.StatusNotFound)
	}

	writeParkingSlotsJsonResponse(w, parkingSlots)
}

// writeParkingSlotJsonResponse function is responsible of parsing parking slot data into JSON format and writing it on
// the given http.ResponseWriter interface.
func writeParkingSlotJsonResponse(w http.ResponseWriter, parkingSlot model.ParkingSlot) {

	bytes, _ := json.Marshal(parkingSlot)

	w.Header().Set("Content-Type", jsonContentType)
	_, _ = w.Write(bytes)
}

// writeParkingSlotsJsonResponse function is responsible of parsing parking slot array data into JSON format and writing
// it on the given http.ResponseWriter interface.
func writeParkingSlotsJsonResponse(w http.ResponseWriter, parkingSlot [] model.ParkingSlot) {

	bytes, _ := json.Marshal(parkingSlot)

	w.Header().Set("Content-Type", jsonContentType)
	_, _ = w.Write(bytes)
}