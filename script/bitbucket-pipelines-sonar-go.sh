#!/usr/bin/env bash

# Setup the import path of the application
BASE_PATH="${GOPATH}/src/bitbucket.org/${BITBUCKET_REPO_OWNER}"
mkdir -p ${BASE_PATH}
export IMPORT_PATH="${BASE_PATH}/${BITBUCKET_REPO_SLUG}"
ln -s ${PWD} ${IMPORT_PATH}

# Create the output folder for test coverage profile
mkdir -p ${IMPORT_PATH}/target

# Build, test and install the packages of the go application
cd ${IMPORT_PATH}/model
go get
go build

cd ${IMPORT_PATH}/service
go get
go build

cd ${IMPORT_PATH}/server
go get
go build
go test -coverprofile=${IMPORT_PATH}/target/server-coverage.out

cd ${IMPORT_PATH}
go install

# Install sonar scanner at /opt directory
cd /opt
apt-get update && apt-get install -y jq unzip
curl --insecure -OL https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.3.0.1492-linux.zip
unzip sonar-scanner-cli-3.3.0.1492-linux.zip

./sonar-scanner-3.3.0.1492-linux/bin/sonar-scanner -Dsonar.projectBaseDir=${IMPORT_PATH} -Dsonar.login=${SONAR_LOGIN}
