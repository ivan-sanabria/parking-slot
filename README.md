# Parking Slot - On boarding 

version 1.0.0 - 27/03/2019

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/parking-slot.svg)](http://bitbucket.org/ivan-sanabria/parking-slot/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/parking-slot.svg)](http://bitbucket.org/ivan-sanabria/parking-slot/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_parking-slot&metric=alert_status)](https://sonarcloud.io/component_measures/metric/alert_status/list?id=ivan-sanabria_parking-slot)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_parking-slot&metric=bugs)](https://sonarcloud.io/component_measures/metric/bugs/list?id=ivan-sanabria_parking-slot)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_parking-slot&metric=coverage)](https://sonarcloud.io/component_measures/metric/coverage/list?id=ivan-sanabria_parking-slot)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_parking-slot&metric=ncloc)](https://sonarcloud.io/component_measures/metric/ncloc/list?id=ivan-sanabria_parking-slot)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_parking-slot&metric=sqale_index)](https://sonarcloud.io/component_measures/metric/sqale_index/list?id=ivan-sanabria_parking-slot)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_parking-slot&metric=vulnerabilities)](https://sonarcloud.io/component_measures/metric/vulnerabilities/list?id=ivan-sanabria_parking-slot)

## Introduction

The scope of this project is to learn how to build a set of APIs using golang and TDD. The problem tries to decentralize 
ticket generation of parking venues using 3 domains:

- Car
- Parking
- Parking Slot

## Requirements

- GO 1.12.x
- IDE for GO (IntelliJ).

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your GO - 1.12.x or higher.
2. Download the source code from repository.
3. Integrate your project with IDE:
    - [IntelliJ](https://www.jetbrains.com/help/go/install-and-set-up-product.html)
4. Open IDE and run the file **main.go**.

## Running Application on Terminal

To run the application on terminal:

1. Verify the version of your GO - 1.12.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    cd model
    go get
    go build
    go install
    
    cd ../service
    go get 
    go build 
    go install
    
    cd ../server
    go get
    go build 
    go install
    
    cd ..
    go build
    
    ./parking-slot
```

## Running Unit Tests with Coverage on Terminal

To run the unit tests on terminal:

1. Verify the version of your GO - 1.12.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    cd server
    go test -cover
```

## Review Documentation using Godoc Locally

To review the documentation of the application locally:

1. Verify the version of your GO - 1.12.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    godoc -http=:6060 &
    open http://localhost:6060/pkg/bitbucket.org/ivan-sanabria/parking-slot/
```

## Test Application Locally

After running the application locally, there are 10 endpoints exposed:

- POST - https://localhost:5000/api/v1/car
- PUT - https://localhost:5000/api/v1/car/{uuid}
- GET - https://localhost:5000/api/v1/car/{uuid}
- GET - https://localhost:5000/api/v1/cars?registration={registration}
- GET - https://localhost:5000/api/v1/cars?color={color}
- GET - https://localhost:5000/api/v1/cars?model={model}

- POST - https://localhost:5001/api/v1/parking
- PUT - https://localhost:5001/api/v1/parking/{uuid}
- GET - https://localhost:5001/api/v1/parking/{uuid}
- GET - https://localhost:5001/api/v1/parkings?latitude={latitude}&longitude={longitude}

- POST - https://localhost:5002/api/v1/parking-slot
- PUT - https://localhost:5002/api/v1/parking-slot/{uuid}
- GET - https://localhost:5002/api/v1/parking-slot/{uuid}
- GET - https://localhost:5002/api/v1/parking-slot?car={car-uuid}
- GET - https://localhost:5002/api/v1/parking-slot?parking={parking-uuid}

To test the endpoints you could use the following commands:

```bash
    curl -X POST -d '{"uuid":"uuid-1", "registration":"AM861", "color":"blue", "model":"golf"}' https://localhost:5000/api/v1/car
    curl -X PUT -d '{"registration":"AM862", "color":"red", "model":"golf"}' https://localhost:5000/api/v1/car/uuid-1
    curl -X GET https://localhost:5000/api/v1/car/uuid-1
    curl -X GET https://localhost:5000/api/v1/cars?registration=AM862
    curl -X GET https://localhost:5000/api/v1/cars?color=red
    curl -X GET https://localhost:5000/api/v1/cars?model=golf
    
    curl -X POST -d '{"uuid":"uuid-1", "slots": 10, "latitude":48.8566, "longitude":2.3522}' https://localhost:5001/api/v1/parking
    curl -X PUT -d '{"slots": 20, "latitude":52.5201, "longitude":13.4050}' https://localhost:5001/api/v1/parking/uuid-1
    curl -X GET https://localhost:5001/api/v1/parking/uuid-1
    curl -X GET https://localhost:5001/api/v1/parkings?latitude=52.5201&longitude=13.4050
    
    curl -X POST -d '{"uuid":"uuid-1", "parking-uuid": "uuid-1", "slotId": 2}' https://localhost:5002/api/v1/parking-slot
    curl -X PUT -d '{"parking-uuid": "uuid-1", "slotId": 1, "carUuid": "uuid-1", "checkIn": 1}' https://localhost:5002/api/v1/parking-slot/uuid-1
    curl -X GET https://localhost:5002/api/v1/parking-slot/uuid-1
    curl -X GET https://localhost:5002/api/v1/parking-slot?car=uuid-1
    curl -X GET https://localhost:5002/api/v1/parking-slot?parking=uuid-1
```

# Contact Information

Email: icsanabriar@googlemail.com