// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Main package responsible of defining which servers are going to be exposed for parking-slot app.
package main

import (
	"bitbucket.org/ivan-sanabria/parking-slot/server"
	"log"
	"net/http"
)

// Main function responsible of exposing the different servers into specific ports.
func main() {

	carServer := server.NewCarServer(nil)

	if err := http.ListenAndServe(":5000", carServer); err != nil {
		log.Fatalf("Car Server could not listen on port 5000 %v", err)
	}

	parkingServer := server.NewParkingServer(nil)

	if err := http.ListenAndServe(":5001", parkingServer); err != nil {
		log.Fatalf("Parking Server could not listen on port 5001 %v", err)
	}

	parkingSlotServer := server.NewParkingSlotServer(nil)

	if err := http.ListenAndServe(":5002", parkingSlotServer); err != nil {
		log.Fatalf("Parking Slot Server could not listen on port 5002 %v", err)
	}
}