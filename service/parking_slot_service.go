// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import "bitbucket.org/ivan-sanabria/parking-slot/model"

// Interface that defines services that parking-slot services could execute on parking-slot application.
type ParkingSlotService interface {
	SaveParkingSlot(parkingSlot model.ParkingSlot)
	UpdateParkingSlot(oldParkingSlot model.ParkingSlot, newParkingSlot model.ParkingSlot)
	GetParkingSlotByUuid(uuid string) (model.ParkingSlot, bool)
	GetParkingSlotByCarUuid(carUuid string) (model.ParkingSlot, bool)
	GetParkingSlotsByParkingUuid(parkingUuid string) ([] model.ParkingSlot, bool)
}