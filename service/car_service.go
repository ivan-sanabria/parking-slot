// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package service defines/ implements the services to decentralize parking slot ticket generation, to fulfill
// requirements for different stakeholders of the parking-slot application.
package service

import "bitbucket.org/ivan-sanabria/parking-slot/model"

// Interface that defines services that user clients could execute on parking-slot application.
type CarService interface {
	SaveCar(car model.Car)
	UpdateCar(oldCar model.Car, newCar model.Car)
	GetCarByUuid(uuid string) (model.Car, bool)
	GetCarsByRegistration(registration string) ([] model.Car, bool)
	GetCarsByColor(color string) ([] model.Car, bool)
	GetCarsByModel(model string) ([] model.Car, bool)
}