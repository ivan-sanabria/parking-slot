// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import "bitbucket.org/ivan-sanabria/parking-slot/model"

// Interface that defines services that parking customers could execute on parking-slot application.
type ParkingService interface {
	SaveParking(parking model.Parking)
	UpdateParking(oldParking model.Parking, newParking model.Parking)
	GetParkingByUuid(uuid string) (model.Parking, bool)
	GetParkingByLocation(latitude float64, longitude float64) ([] model.Parking, bool)
}