// Copyright 2019 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package model implements the data structure used to decentralize parking slot ticket generation defined as current
// problem on the automobile sector.
package model

// Structure is used to store car data in memory, that is provided by the user clients.
type Car struct {
	Uuid string `json:"uuid"`
	Registration string `json:"registration"`
	Color string `json:"color"`
	Model string `json:"model"`
}